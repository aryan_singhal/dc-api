const express = require("express");
// const connectDB = require('./config/db');
const path = require("path");
const mongoose = require("mongoose");
const cors = require("cors");
const helmet = require("helmet");
require("dotenv").config();

const app = express();

app.use(express.json());
app.use(helmet());
app.use(
	cors({
		origin:
			process.env.NODE_ENV === "production"
				? "https://devchirps.netlify.com"
				: "http://localhost:3000",
		credentials: true,
	})
);

let count = 0;
async function connectDB() {
	try {
		await mongoose.connect(process.env.MONGO_URI, {
			useUnifiedTopology: true,
			useNewUrlParser: true,
		});
		count = 0;
		console.info("Connected to DB");
	} catch (error) {
		count++;
		console.error("Mongo Error: \n", error);
		if (count < 6) {
			console.log("counter: ", count);
			setTimeout(connectDB, 3000);
		}
	}
}
connectDB();

app.get("/", (req, res) => {
	res.send("hello world");
});

app.use("/api/users", require("./routes/api/users"));
app.use("/api/auth", require("./routes/api/auth"));
app.use("/api/profile", require("./routes/api/profile"));
app.use("/api/posts", require("./routes/api/posts"));

//Serve static assests in production
// if (process.env.NODE_ENV === 'production') {
//   //set static folder
//   app.use(express.static('client/build'));

//   app.get('*', (req, res) => {
//     res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
//   });
// }

const PORT = process.env.PORT;

app.listen(PORT, () => {
	console.log(`Server started at ${PORT}`);
});
