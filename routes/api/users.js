const express = require("express");
const router = express();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const gravatar = require("gravatar");
const User = require("../../models/Users");

const { check, validationResult } = require("express-validator");
const algoliasearch = require("algoliasearch");

const client = algoliasearch("0VAIUJZWVG", "d3900ace5cefc8b876235692803df778");
const index = client.initIndex("users");

router.post(
	"/",
	[
		check("name", "Name is Required").not().isEmpty(),
		check("email", "Please enter a valid email").isEmail(),
		check(
			"password",
			"Please enter a password with 6 or more characters"
		).isLength({
			min: 8,
		}),
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
			});
		}
		const { name, email, password } = req.body;

		try {
			//see if user exists
			let user = await User.findOne({
				email,
			});

			if (user) {
				return res.status(400).json({
					errors: [
						{
							msg: "User already exists",
						},
					],
				});
			}

			//get user gravatar
			const avatar = gravatar.url(email, {
				s: "200",
				r: "pg",
				d: "mm",
			});

			user = new User({
				name,
				email,
				avatar,
				password,
			});

			//encrypt password
			const salt = await bcrypt.genSalt(10);

			user.password = await bcrypt.hash(password, salt);

			await user.save();
			let record = { name: user.name };
			index
				.saveObject(record, { autoGenerateObjectIDIfNotExist: true })
				.wait()
				.then((res) => {
					console.log(res);
				})
				.catch((err) => {
					console.error(err);
				});

			//return jwt
			const payload = {
				user: {
					id: user.id,
				},
			};

			jwt.sign(
				payload,
				process.env.SECRET,
				{
					expiresIn: 360000,
				},
				(err, token) => {
					if (err) {
						throw err;
					}
					res.json({
						token,
					});
				}
			);

			// res.send('User registered')
		} catch (err) {
			console.error(err.message);

			return res.status(500).json("Server Error");
		}
	}
);

module.exports = router;
