FROM node:14-alpine

WORKDIR /usr/app

COPY package.json ./
COPY package-lock.json ./

RUN npm ci

COPY . .
COPY .env.prod .env

ENV NODE_ENV production

EXPOSE 8080

CMD ["node", "server"]

USER node
